using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using NUnit.Framework;

namespace SplatLeague.Server.Tests;

[TestFixture]
public abstract class TestFixtureBase {

	[OneTimeSetUp]
	public void OneTimeSetUp_Base() {
		JsonConvert.DefaultSettings = GetJsonConverterSettings;
	}

	private static JsonSerializerSettings GetJsonConverterSettings() {
		JsonSerializerSettings options = new() {
			DateFormatHandling = DateFormatHandling.IsoDateFormat,
			MissingMemberHandling = MissingMemberHandling.Ignore,
			NullValueHandling = NullValueHandling.Include,
			ReferenceLoopHandling = ReferenceLoopHandling.Ignore,
			PreserveReferencesHandling = PreserveReferencesHandling.None
		};
		options.Converters.Add( new StringEnumConverter {
			AllowIntegerValues = false
		} );
		return options;
	}

}
