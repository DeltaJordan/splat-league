import { html, TemplateResult } from 'lit';
import '../src/splatoon-league.js';

export default {
  title: 'SplatoonLeague',
  component: 'splatoon-league',
  argTypes: {
    backgroundColor: { control: 'color' },
  },
};

interface Story<T> {
  (args: T): TemplateResult;
  args?: Partial<T>;
  argTypes?: Record<string, unknown>;
}

interface ArgTypes {
  title?: string;
  backgroundColor?: string;
}

const Template: Story<ArgTypes> = ({ title, backgroundColor = 'white' }: ArgTypes) => html`
  <splatoon-league style="--splatoon-league-background-color: ${backgroundColor}" .title=${title}></splatoon-league>
`;

export const App = Template.bind({});
App.args = {
  title: 'My app',
};
